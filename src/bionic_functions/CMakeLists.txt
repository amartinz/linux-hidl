add_subdirectory(async_safe)

add_library(bionic_functions STATIC
    system_properties.cpp
)

target_include_directories(bionic_functions PUBLIC include)
target_link_libraries(bionic_functions async_safe)

install(DIRECTORY include/sys DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/linux-hidl)
